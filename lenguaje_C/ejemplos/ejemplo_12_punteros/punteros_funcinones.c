#include <stdio.h>


int funcion(float *x)
{
    *x = 1.0;

    return 0;
}


int main(void)
{
    float x = 0.0;

    funcion(&x);

    printf("x = %f\n", x);


   return 0;
}
