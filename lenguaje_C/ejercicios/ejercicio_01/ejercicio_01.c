#include<stdio.h>
typedef enum {false, true} bool;


float funcion1(float n)
{
    return n*n;
}


float funcion2(float n)
{
    return n*n + 1000*n;
}


float funcion3(float n)
{
    float resultado = 0.0;

    if(n <= 0)
    {
        resultado = 10;
    }
    else if(n > 0 && n < 5)
    {
        resultado = n*n - n + 1;
    }
    else if(n >= 5)
    {
        resultado = 2*n - 1;
    }

    return resultado;
}


int main(void)
{
    int funcion;

    float n;
    float resultado = 0.0;

    bool imprimir = true;


    printf("Ingrese un número decimal: \n");
    scanf("%f", &n);


    printf("\nIngrese:\n");
    printf("1, para seleccionar f1(n)\n");
    printf("2, para seleccionar f2(n)\n");
    printf("3, para seleccionar f3(n)\n");
    scanf("%d", &funcion);


    switch(funcion)
    {
        case 1:
            resultado = funcion1(n);
            break;
        case 2:
            resultado = funcion2(n);
            break;
        case 3:
            resultado = funcion3(n);
            break;
        default:
            imprimir = false;
            printf("\nSe elección no es válida.\n");
            break;
    }


    if(imprimir == true)
        printf("\nEl resultado de la operación es: %f.\n", resultado);


    return 0;
}

