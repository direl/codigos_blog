#include <stdio.h>
#define SIZE 2


int main(void)
{
    unsigned i=0, j=0;

    float matriz[SIZE][SIZE];
    float det_matriz = 0.0;
    float inversa[SIZE][SIZE];


    printf("Ingrese los elementos de una matriz 2x2 para obtener su inversa: \n\n");

    for(i=0; i<SIZE; i++)
    {
        for(j=0; j<SIZE; j++)
        {
            printf("matriz[%d][%d]: ",i , j);
            scanf("%f", &matriz[i][j]);
        }
    }

    printf("\n\n La matriz ingresada es: \n\n");

    for(i=0; i<SIZE; i++)
    {
        for(j=0; j<SIZE; j++)
        {
            printf("\t %f ", matriz[i][j]);
        }

        printf("\n");

    }


    det_matriz = matriz[0][0]*matriz[1][1] - matriz[0][1]*matriz[1][0];

    inversa[0][0] =  matriz[1][1]/det_matriz;
    inversa[0][1] = -matriz[0][1]/det_matriz;
    inversa[1][0] = -matriz[1][0]/det_matriz;
    inversa[1][1] =  matriz[0][0]/det_matriz;


    printf("\n\n La matriz inversaa es: \n\n");

    for(i=0; i<SIZE; i++)
    {
        for(j=0; j<SIZE; j++)
        {
            printf("\t %f ", inversa[i][j]);
        }

        printf("\n");

    }


    return 0;
}
